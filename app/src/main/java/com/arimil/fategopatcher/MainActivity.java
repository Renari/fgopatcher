package com.arimil.fategopatcher;

import android.content.DialogInterface;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.stericson.RootShell.RootShell;

import java.io.File;


public class MainActivity extends AppCompatActivity {
    public static final String DATABASE_PATH = Environment.getExternalStorageDirectory() + "/FGOTLDB.db";
    TextView outputTextView;
    ScrollView scrollView;
    Button installButton;
    Button restoreButton;
    Button updateButton;

    public void output(final String str) {
        outputTextView.append(str + System.getProperty("line.separator"));
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    void disableButtons() {
        installButton.setEnabled(false);
        restoreButton.setEnabled(false);
        updateButton.setEnabled(false);
    }

    void enableButtons() {
        installButton.setEnabled(true);
        restoreButton.setEnabled(true);
        updateButton.setEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        outputTextView = (TextView) findViewById(R.id.outputTextView);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        installButton = (Button) findViewById(R.id.Install);
        updateButton = (Button) findViewById(R.id.Update);
        restoreButton = (Button) findViewById(R.id.Restore);
        if (!RootShell.isAccessGiven()) {
            Toast.makeText(MainActivity.this, "Root is required to run this application.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void installButtonClick(View v) {
        new Patcher(this).execute(true);
    }

    public void restoreButtonClick(View v) {
        new Patcher(this).execute(false);
    }

    public void updateButtonCLick(View v) {
        final File databaseFile = new File(DATABASE_PATH);
        if (databaseFile.exists()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Delete existing database?");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    MainActivity.this.deleteDatabase(DATABASE_PATH);
                    new UpdateDatabase(MainActivity.this).execute();
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    new UpdateDatabase(MainActivity.this).execute();
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Nullable
    static String[] getTranslationFields(String filename) {
        switch (filename) {
            case "mstAi":
                return new String[]{"infoText"};
            case "mstBankShop":
                return new String[]{"name", "numDetail", "priceDetail"};
            case "mstBuff":
                return new String[]{"name", "detail"};
            case "mstClass":
                return new String[]{"name"};
            case "mstCommandSpell":
                return new String[]{"name", "detail"};
            case "mstCv":
                return new String[]{"name"};
            case "mstEquip":
                return new String[]{"name", "detail"};
            case "mstEvent":
                return new String[]{"name", "detail"};
            case "mstFunc":
                return new String[]{"popupText"};
            case "mstGacha":
                return new String[]{"name"};
            case "mstIllustrator":
                return new String[]{"name", "comment"};
            case "mstItem":
                return new String[]{"name", "detail"};
            case "mstQuest":
                return new String[]{"name"};
            case "mstShop":
                return new String[]{"name", "detail"};
            case "mstSkill":
                return new String[]{"name", "ruby"};
            case "mstSkillDetail":
                return new String[]{"detail"};
            case "mstSpot":
                return new String[]{"name"};
            case "mstSvt":
                return new String[]{"name", "ruby", "battleName"};
            case "mstSvtComment":
                return new String[]{"comment"};
            case "mstTreasureDevice":
                return new String[]{"name", "ruby", "rank", "typeText"};
            case "mstTreasureDeviceDetail":
                return new String[]{"detail", "detailShort"};
            case "mstUserExp":
                return new String[]{"comment"};
            case "mstWar":
                return new String[]{"name"};
            case "mstSvtFollower":
                return new String[]{"name"};
            default:
                return null;
        }
    }
}