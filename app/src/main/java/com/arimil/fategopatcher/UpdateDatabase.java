package com.arimil.fategopatcher;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateDatabase extends AsyncTask<Void, String, String> {

    MainActivity mainActivity;

    UpdateDatabase(MainActivity activity) {
        mainActivity = activity;
    }

    @Override
    protected String doInBackground(Void... params) {
        final SQLiteDatabase db = mainActivity.openOrCreateDatabase(mainActivity.DATABASE_PATH, mainActivity.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS Text (file TEXT, field Text, originaltext TEXT, translatedtext TEXT);");
        MasterData masterData = new MasterData();
        String[] files = masterData.getMasterDataList();
        for (String file : files) {
           JSONArray json = masterData.getMasterData(file);
            String[] fields = mainActivity.getTranslationFields(file);
            if (fields == null) {
                continue;
            }
            for (int i = 0; i < json.length(); i++) {
                try {
                    JSONObject entry = json.getJSONObject(i);
                    for (String field : fields) {
                        String string = entry.getString(field);
                        if (!string.equals("") && !string.equals("-")) {
                            Cursor c = db.rawQuery("SELECT file FROM Text WHERE file = '" + file +
                                    "' AND field = '" + field + "' AND (originaltext = '" + string +
                                    "' OR translatedtext = '" + string + "');", null);
                            if (c.getCount() == 0) {
                                publishProgress("Found translatable string: " + string);
                                db.execSQL("INSERT INTO 'Text'('file', 'field', 'originaltext') VALUES('" + file + "', '" + field + "', '" + string + "');");
                            }
                            c.close();
                        }
                    }
                } catch (JSONException e) {
                    Log.e("FGOPatcher", "JSON error in " + file);
                    publishProgress("JSON error in " + file);
                    break;
                }
            }
        }
        return "Finished!";
    }

    @Override
    protected void onPreExecute() {
        mainActivity.disableButtons();
    }

    @Override
    protected void onPostExecute(String result) {
        mainActivity.output(result);
        mainActivity.enableButtons();
    }

    @Override
    protected void onProgressUpdate(String... update) {
        mainActivity.output(update[0]);
    }
}
