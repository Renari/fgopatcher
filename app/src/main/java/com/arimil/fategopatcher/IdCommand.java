package com.arimil.fategopatcher;

import com.stericson.RootShell.execution.Command;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IdCommand extends Command {

    public String aid = null;

    public IdCommand(int id, String... command) {
        super(id, command);
    }

    @Override
    public void commandOutput(int id, String line) {
        super.commandOutput(id, line);
        Pattern p = Pattern.compile("\\s*[-rwx]*\\s*(\\d+)\\s*\\d+\\s*\\d+\\s*[\\d-]*\\s*[\\d:]*\\s(?:.*)\\.dat");
        Matcher m = p.matcher(line);
        if(m.find()) {
            aid = m.group(1);
        }
    }
}
