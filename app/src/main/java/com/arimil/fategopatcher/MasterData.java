package com.arimil.fategopatcher;

import android.os.Environment;
import android.util.Log;

import com.stericson.RootShell.execution.Command;
import com.stericson.RootTools.RootTools;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MasterData {

    public final String MASTER_DATA = "masterData.dat";
    public final String MASTER_DATA_DIR = Environment.getDataDirectory() + "/data/com.aniplex.fategrandorder/files/MasterDataCaches";
    public final String MASTER_DATA_LIST = "masterDataList.dat";

    private Map<String, JSONArray> masterData = new HashMap<>();
    private List<String> masterDataList = new ArrayList<>();
    private String[] versionData;

    MasterData() {
        decryptDatFiles();
    }

    public String[] getVersionData() {
        return versionData;
    }

    public String[] getMasterDataList() {
        return masterDataList.toArray(new String[masterDataList.size()]);
    }

    public JSONArray getMasterData(String filename) {
        return masterData.get(filename);
    }

    public void updateMasterData(String filename, JSONArray data) {
        masterData.put(filename, data);
    }
    
    private static String[] removeEmptyEntries(String[] array) {
        List<String> entries = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (array[i].trim().length() != 0) {
                entries.add(array[i]);
            }
        }
        return entries.toArray(new String[entries.size()]);
    }

    private void decryptDatFiles() {
        File masterDataFile = new File(MASTER_DATA_DIR + "/" + MASTER_DATA);
        File masterDataListFile = new File(MASTER_DATA_DIR + "/" + MASTER_DATA_LIST);
        Command command = new Command(0,
                "cp " + masterDataListFile + " " + Environment.getExternalStorageDirectory(),
                "cp " + masterDataFile + " " + Environment.getExternalStorageDirectory());
        masterDataFile = new File(Environment.getExternalStorageDirectory() + "/" + MASTER_DATA);
        masterDataListFile = new File(Environment.getExternalStorageDirectory() + "/" + MASTER_DATA_LIST);
        try {
            int timeout = 20000;
            int time = 0;
            RootTools.getShell(true).add(command);
            while (!masterDataListFile.exists() || !masterDataFile.exists()) {
                time += 40;
                if (time < timeout) {
                    Thread.sleep(40);
                } else {
                    Log.e("FGOPatcher", "Copy operation timed out.");
                    return;
                }
            }
            InputStream is = new FileInputStream(masterDataListFile);
            byte[] data = IOUtils.toByteArray(is);
            masterDataListFile.delete();
            String list = CryptData.decrypt(data, CryptData.CKEY2, CryptData.CVEC2);
            //get index of first new line
            int fnl = list.indexOf('\n');
            //get file crc from first line
            //String filecrc = list.substring(1, fnl);
            //remove file crc
            list = list.substring(fnl+1);
            String[] dataList = removeEmptyEntries(list.split("\n"));
            versionData = dataList[0].split(",");
            is = new FileInputStream(masterDataFile);
            data = IOUtils.toByteArray(is);
            for(int i = 1; i < dataList.length; i++) {
                String[] fileinfo = dataList[i].split(",");
                long mdk = CryptData.getMdk(fileinfo[0], Integer.parseInt(versionData[1]));
                String filetext = CryptData.decrypt(data, CryptData.CKEY2, CryptData.CVEC2, true, mdk, true);
                JSONArray json = new JSONArray(filetext);
                //remove read data
                int leblen = CryptData.getLeb128Length(data);
                int filelen = CryptData.readUnsignedLeb128(data);
                data = Arrays.copyOfRange(data, leblen + filelen, data.length);
                masterData.put(fileinfo[0], json);
                masterDataList.add(fileinfo[0]);
            }
            masterDataFile.delete();
        } catch (Exception e) {
            Log.e("FGOPatcher", "Unable to get master data");
        }
    }
}
