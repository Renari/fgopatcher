package com.arimil.fategopatcher;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.stericson.RootShell.execution.Command;
import com.stericson.RootTools.RootTools;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class Patcher extends AsyncTask<Boolean, String, String> {

    MainActivity mainActivity;

    Patcher(MainActivity activity) {
        mainActivity = activity;
    }

    @Override
    protected String doInBackground(Boolean... patchmode) {
        if (patchmode[0] == true) {
            publishProgress("Patching...");
        } else {
            publishProgress("Restoring...");
        }
        File database = new File(mainActivity.DATABASE_PATH);
        if (!database.exists()) {
            publishProgress("Unable to find FGOTLDB.");
            return "Failed";
        }
        SQLiteDatabase db = mainActivity.openOrCreateDatabase(mainActivity.DATABASE_PATH, mainActivity.MODE_PRIVATE, null);
        MasterData masterData = new MasterData();
        Cursor files = db.rawQuery("SELECT DISTINCT file FROM Text WHERE translatedtext NOT NULL", null);
        while (files.moveToNext()) {
            int index = files.getColumnIndex("file");
            String file = files.getString(index);
            publishProgress(file);
            String[] fields = mainActivity.getTranslationFields(file);
            if (fields == null) {
                continue;
            }
            publishProgress("Found translations in " + file);
            JSONArray json = masterData.getMasterData(file);
            for (String field : fields) {
                try {
                    Cursor c = db.rawQuery("SELECT * FROM Text WHERE file = '" + file + "'" +
                            " AND field = '" + field + "' AND translatedtext NOT NULL;", null);
                    if (c.getCount() == 0) {
                        continue;
                    }
                    while(c.moveToNext()) {
                        String originaltext = c.getString(c.getColumnIndex("originaltext"));
                        String translatedtext = c.getString(c.getColumnIndex("translatedtext"));
                        for (int i = 0; i < json.length(); i++) {
                            JSONObject entry = json.getJSONObject(i);
                            String text = entry.getString(field);
                            if (patchmode[0] == true) { //patch
                                if (text.equals(originaltext)) {
                                    publishProgress("Replacing " + originaltext + " with " + translatedtext);
                                    entry.put(field, translatedtext);
                                }
                            }
                            else { //restore
                                if (text.equals(translatedtext)) {
                                    publishProgress("Replacing " + translatedtext + " with " + originaltext);
                                    entry.put(field, originaltext);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e("FGOPatcher", "JSON error in " + file);
                    publishProgress("JSON error in " + file);
                    return "Failed";
                }
            }
            masterData.updateMasterData(file, json);
        }
        String[] versionData = masterData.getVersionData();
        int versionNumber = Integer.parseInt(versionData[1]);
        String masterDataList = versionData[0]+","+versionData[1]+"\n";
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (String file : masterData.getMasterDataList()) {
            JSONArray json = masterData.getMasterData(file);
            publishProgress("Encrypting: "+file);
            try {
                long mdk = CryptData.getMdk(file, versionNumber);
                byte[] encrypted = CryptData.encrypt(json.toString(), CryptData.CKEY2, CryptData.CVEC2, mdk, true);
                long filecrc = CryptData.getCrc(encrypted);
                masterDataList += file + "," + Long.toString(filecrc) + "\n";
                encrypted = CryptData.base64Encode(encrypted);
                encrypted = CryptData.prependLeb128(encrypted);
                out.write(encrypted);
            } catch (Exception e) {
                Log.e("FGOPatcher", "Encryption error");
                publishProgress("Encryption error");
                return "Failed";
            }
        }
        File dataOutput = new File(Environment.getExternalStorageDirectory()+"/masterData.dat");
        File listOutput = new File(Environment.getExternalStorageDirectory()+"/masterDataList.dat");
        try {
            FileOutputStream os = new FileOutputStream(dataOutput);
            os.write(out.toByteArray());
            os.close();
            os = new FileOutputStream(listOutput);
            masterDataList = "~" + Long.toString(CryptData.getCrc(masterDataList.getBytes("UTF-8"))) + "\n" + masterDataList;
            byte[] mdle = CryptData.encrypt(masterDataList, CryptData.CKEY2, CryptData.CVEC2, true);
            mdle = CryptData.base64Encode(mdle);
            os.write(mdle);
            os.close();
        } catch (Exception e) {
            Log.e("FGOPatcher", "Write error");
            publishProgress("Write error");
            dataOutput.delete();
            listOutput.delete();
            return "Failed";
        }
        IdCommand idCmd = new IdCommand(4, "ls -l -n " + Environment.getDataDirectory() +
                "/data/com.aniplex.fategrandorder/files/MasterDataCaches",
                "am force-stop com.aniplex.fategrandorder");
        try {
            RootTools.getShell(true).add(idCmd);
            int timeout = 20000;
            int time = 0;
            while(idCmd.aid == null) {
                if (time > timeout) {
                    Log.e("FGOPatcher", "Failed to get guid/uid");
                    publishProgress("Failed to get guid/uid");
                    dataOutput.delete();
                    listOutput.delete();
                    return "Failed";
                }
                time += 40;
                Thread.sleep(40);
            }
            publishProgress("guid/uid: " + idCmd.aid);
            String[] commands = {
                "cp " + Environment.getExternalStorageDirectory() + "/" + masterData.MASTER_DATA +
                        " " + masterData.MASTER_DATA_DIR,
                "cp " + Environment.getExternalStorageDirectory() + "/" + masterData.MASTER_DATA_LIST +
                        " " + masterData.MASTER_DATA_DIR,
                "cd " + masterData.MASTER_DATA_DIR,
                "chown " + idCmd.aid + "_" + idCmd.aid + " *.dat",
                "chmod -R 770 ./"
            };
            Command move = new Command(5, commands);
            RootTools.getShell(true).add(move);
        } catch (Exception e) {
            Log.e("FGOPatcher", "Unable to update changes");
            publishProgress("Unable to update changes");
            return "Failed";
        }
        return "Finished!";
    }

    @Override
    protected void onPreExecute() {
        mainActivity.disableButtons();
    }

    @Override
    protected void onPostExecute(String result) {
        mainActivity.output(result);
        mainActivity.enableButtons();
    }

    @Override
    protected void onProgressUpdate(String... update) {
        mainActivity.output(update[0]);
    }
}
